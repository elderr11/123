import java.util.Scanner;

public class Calculator {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
      int num1 = getNum();
      int num2 = getNum();
      char operation = getOperation();
      int result = calculate(num1,num2,operation);
        System.out.println("number1 = " + num1 + " " + "number2 = " + num2 + " " + "operator = " + operation + " " + "result = " + result);

    }
    public static int getNum(){
        System.out.println("Введите число: ");
        int num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else{
            System.out.println("Недопустимое значение. Повторите ввод.");
            scanner.next();
            num = getNum();
        }
        return num;
    }
    public static char getOperation(){
        System.out.println("Введите символ операции: ");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else{
            System.out.println("Недопустимое значение. Повторите ввод.");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }
    public static int calculate(int num1, int num2, char operation){
        int result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '/':
                result = num1/num2;
                break;
            case '*':
                result = num1*num2;
                break;
                default:
                    System.out.println("Неизвестная операция. Повторите попытку.");
                    result = calculate(num1,num2,getOperation());
        }
        return result;
    }
}
